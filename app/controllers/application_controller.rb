class ApplicationController < ActionController::API
  include ::ActionController::Serialization

  after_action :set_access_control_headers

  attr_reader :current_user

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: {error: exception.message}, status: :not_found
  end
  rescue_from ActionController::UnknownFormat do |exception|
    render json: {message: "UnknownFormat"}, status: :not_acceptable
  end
  rescue_from ActionController::RoutingError do |exception|
    render json: {message: "RoutingError"}, status: :not_found
  end
  rescue_from ActionController::UnknownController do |exception|
    render json: {message: "UnknownController"}, status: :not_found
  end
  rescue_from ActionController::ParameterMissing do |exception|
    render json: {error: exception.message}, status: :bad_request
  end

  serialization_scope :view_context

  def preflight
    head :no_content
  end

  def main
    render json: {message: "Welcome to Job Api. Please use the routes file."}
  end

  private
    def set_access_control_headers
      headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, DELETE, OPTIONS'
      headers['Access-Control-Expose-Headers'] = 'X-Total-Count, X-Total-Pages, Link'
      headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token'
      headers['Access-Control-Allow-Origin'] = '*'
    end

    def http_token
      @http_token ||= if (request.headers['Authorization'].present?)
        request.headers['Authorization'].split(' ').last
      end
    end

    def auth_token
      @auth_token ||= JsonWebToken.decode(http_token)
    end

    def admin_session_datum_id_in_token?
      http_token && auth_token && auth_token[:admin_session_datum_id].to_i
    end

    def admin_id_in_token?
      http_token && auth_token && auth_token[:admin_id].to_i
    end

    def user_id_in_token?
     http_token && auth_token && auth_token[:user_id].to_i
    end

    def user_session_datum_id_in_token?
      http_token && auth_token && auth_token[:user_session_datum_id].to_i
    end
end
