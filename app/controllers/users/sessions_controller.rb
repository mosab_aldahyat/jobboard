class Users::SessionsController < Users::UserApplicationController
  skip_before_action :user_authenticate_request!, only: [:create]

  def create
    user_can_login = User.can_login(params)
    if user_can_login
      user = User.find_by_email(params[:email])
      user_session_datum = create_user_session_datum(user.id)
      render json: user_payload(user, user_session_datum)
    else
      render json: CustomErrors.create_error("Incorrect Email or Password"), status: :bad_request
      return
    end
  end

  def destroy
    current_user_session_datum.destroy
    render json: {message: "you have been logged out"}
  end
end
