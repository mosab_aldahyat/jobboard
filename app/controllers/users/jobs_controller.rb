class Users::JobsController < Users::UserApplicationController
  before_action :set_job, only: [:show]

  def index
    @jobs = Job.active.not_expired.order(:id)
    render json: @jobs
  end

  def show
    render json: @job
  end

  def search
    render json: Job.search(params).not_expired
  end

  private
    def set_job
      @job = Job.find(params[:id])
    end
end
