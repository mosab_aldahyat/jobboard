class Users::JobApplicationsController < Users::UserApplicationController
  before_action :set_job_application, only: [:show]

  def index
    @job_applications = JobApplication.all.order(:id)
    render json: @job_applications, include: [:user, :job]
  end

  def create
    @job_application = JobApplication.new(job_application_params.merge!(user_id: current_user.id))
    if @job_application.save
      render json: @job_application , include: [:user, :job], status: :created
    else
      render json: @job_application.errors, status: :unprocessable_entity
    end
  end

  def show
    if @job_application.user_id == current_user.id
      render json: @job_application, include: [:user, :job]
      return
    end
    render json: {message: "you can't view this job"} , status: :forbidden
  end

  private
    def set_job_application
      @job_application = JobApplication.find(params[:id])
    end

    def job_application_params
      params.permit(:job_id)
    end
end
