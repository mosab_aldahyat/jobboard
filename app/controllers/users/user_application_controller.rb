class Users::UserApplicationController < ApplicationController

  attr_reader :current_user

  before_action :user_authenticate_request!

  def current_user
    unless auth_token.present?
      return nil
    end
    @current_user ||= User.find(auth_token[:user_id])
  end

  def current_user_session_datum
    unless auth_token.present?
      return nil
    end
    @current_user_session_datum ||=  UserSessionDatum.find_by(id: auth_token[:user_session_datum_id])
  end

  protected
    def user_authenticate_request!
      unless user_id_in_token? && user_session_datum_id_in_token?
        render json: { error: 'Not Authenticated' }, status: :unauthorized
        return
      end
      @current_user = User.find(auth_token[:user_id])
      @current_user_session_datum =  UserSessionDatum.find_by(id: auth_token[:user_session_datum_id])
      if @current_user.nil? || !@current_user_session_datum.present?
        render json: { error: 'Not Authenticated' }, status: :unauthorized
        return
      end
      rescue JWT::VerificationError, JWT::DecodeError
      render json: { error: 'Not Authenticated' }, status: :unauthorized
    end

    def user_payload user, user_session_datum
      return nil unless user.present? && user_session_datum.present?
      user_json = User::UserSerializer.new(user).as_json
      data = {
        auth_token:  "Bearer " + JsonWebToken.encode({ user_id: user.id , user_session_datum_id: user_session_datum.id }),
        user: user_json
      }
    end

    def create_user_session_datum user_id
      begin
        user_session_datum = UserSessionDatum.create(user_id: user_id)
      rescue => e
        return nil
      end
    end
end
