class Users::UsersController < Users::UserApplicationController
  skip_before_action :user_authenticate_request!, only: [:create]

  def create
    user = User.create_new_user(params)
    if user.present?
      if user.valid?
        user_login_data = login_user(params)
        if user_login_data.present?
          render json: user_login_data
          return
        end
      else
        render json: user.errors, status: :bad_request
        return
      end
      render json: CustomErrors.create_error("Couldn't log you in."), status: :bad_request
    else
      render json: CustomErrors.create_error("Couldn't create user."), status: :bad_request
    end
  end

  private
    def login_user params
      user_can_login = User.can_login(params)
      if user_can_login
        user = User.find_by_email(params[:email])
        user_session_datum = create_user_session_datum(user.id)
        user_payload(user, user_session_datum)
      else
        nil
      end
    end
end
