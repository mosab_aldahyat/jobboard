class Admins::JobApplicationsController < Admins::AdminApplicationController
  before_action :set_job_application, only: [:show]

  def index
    @job_applications = JobApplication.all.order(:id)
    render json: @job_applications, include: [:user, :job]
  end

  def show
    @job_application.seen
    render json: @job_application, include: [:user, :job]
  end

  private
    def set_job_application
      @job_application = JobApplication.find(params[:id])
    end
end
