class Admins::AdminApplicationController < ApplicationController

  attr_reader :current_admin

  before_action :admin_authenticate_request!

  def current_admin
    unless auth_token.present?
      return nil
    end
    @current_admin ||= Admin.find(auth_token[:admin_id])
  end

  def current_admin_session_datum
    unless auth_token.present?
      return nil
    end
    @current_admin_session_datum ||=  AdminSessionDatum.find_by(id: auth_token[:admin_session_datum_id])
  end

  protected
    def admin_authenticate_request!
      unless admin_id_in_token? && admin_session_datum_id_in_token?
        render json: { error: 'Not Authenticated' }, status: :unauthorized
        return
      end
      @current_admin = Admin.find(auth_token[:admin_id])
      @current_admin_session_datum =  AdminSessionDatum.find_by(id: auth_token[:admin_session_datum_id])
      if @current_admin.nil? || !@current_admin_session_datum.present?
        render json: { error: 'Not Authenticated' }, status: :unauthorized
        return
      end
      rescue JWT::VerificationError, JWT::DecodeError
      render json: { error: 'Not Authenticated' }, status: :unauthorized
    end


    def admin_payload admin, admin_session_datum
      return nil unless admin.present? && admin_session_datum.present?
      admin_json = Admin::AdminSerializer.new(admin).as_json
      data = {
        auth_token: "Bearer " + JsonWebToken.encode({admin_id: admin.id, admin_session_datum_id: admin_session_datum.id}),
        admin: admin_json
      }
    end
end
