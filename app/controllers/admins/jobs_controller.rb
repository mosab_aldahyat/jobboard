class Admins::JobsController < Admins::AdminApplicationController
  before_action :set_job, only: [:show, :update, :destroy]

  def index
    @jobs = Job.active.order(:id)
    render json: @jobs
  end

  def create
    @job = Job.new(job_params)
    if @job.save
      render json: @job, status: :created
    else
      render json: @job.errors, status: :unprocessable_entity
    end
  end

  def show
    render json: @job
  end

  def update
    if @job.update(job_params)
      render json: @job
    else
      render json: @job.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @job.destroy
    render json: {message: "Deleted."}
  end

  def search
    render json: Job.search(params)
  end

  private
    def set_job
      @job = Job.find(params[:id])
    end

    def job_params
      params.permit(:title, :description, :expire_date)
    end
end
