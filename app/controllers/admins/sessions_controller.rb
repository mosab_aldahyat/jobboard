class Admins::SessionsController < Admins::AdminApplicationController
  skip_before_action :admin_authenticate_request!, only: [:create]

  def create
    admin_can_login = Admin.can_login(params)
    if admin_can_login
      admin = Admin.find_by_email(params[:email])
      admin_session_datum = create_admin_session_datum(admin.id)
      render json: admin_payload(admin, admin_session_datum)
    else
      render json: CustomErrors.create_error("Incorrect Email or Password"), status: :bad_request
      return
    end
  end

  def destroy
    current_admin_session_datum.destroy
    render json: {message: "you have been logged out"}
  end

  private
    def create_admin_session_datum admin_id
      begin
        admin_session_datum = AdminSessionDatum.create(admin_id: admin_id)
      rescue => e
        return nil
      end
    end
end
