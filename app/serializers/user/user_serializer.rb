class User::UserSerializer < ActiveModel::Serializer
  attributes :id, :email
end
