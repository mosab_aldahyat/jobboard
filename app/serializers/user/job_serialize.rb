class User::JobSerializer < ActiveModel::Serializer
  attributes :id, :title, :description
end
