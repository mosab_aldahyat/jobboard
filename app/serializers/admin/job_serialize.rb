class Admin::JobSerializer < ActiveModel::Serializer
  attributes :id, :title, :description
end
