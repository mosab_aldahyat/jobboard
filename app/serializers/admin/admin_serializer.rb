class Admin::AdminSerializer < ActiveModel::Serializer
  attributes :id, :email
end
