class AdminSessionDatum < ApplicationRecord
  belongs_to :admin
  after_commit :check_for_duplicate_session, on: :create

  def check_for_duplicate_session
    data = AdminSessionDatum.where(admin_id: self.admin_id).where.not(id: self.id)
    data.each do |session|
      session.destroy if(session.id != self.id)
    end
  end
end
