class JobApplication < ApplicationRecord
  belongs_to :user
  belongs_to :job

  validates :status, inclusion: { in: ["Not Seen", "Seen"], message: "\"Status\" must either be Not Seen or Seen" }
  validates_uniqueness_of :user, scope: :job, message: "you already applied to this job"

  def seen
    self.status = "Seen"
    if  self.status_was == "Not Seen"
      SendgridLib.send_email(self.user.email, nil,
                             "Your JobApplication has been seen!",
                             "Your JobApplication has been seen! Yahoo.")
    end
    self.save
  end
end
