class Admin < ApplicationRecord
  has_one :admin_session_datum

  validates_uniqueness_of :email, message: "Email is already exists"

  has_secure_password

  def self.can_login params
    admin = self.find_by_email(params[:email])
    admin.present? && admin.authenticate(params[:password])
  end
end
