class User < ApplicationRecord
  has_many :job_applications
  has_one :user_session_datum

  validates_uniqueness_of :email, message: "Email is already exists"

  has_secure_password

  def self.create_new_user params
    email = params[:email]
    password = params[:password]
    return nil unless email.present? && password.present?
    begin
      user = User.create(email: email, password: password)
    rescue => e
      return nil
    end
  end

  def self.can_login params
    user = self.find_by_email(params[:email])
    user.present? && user.authenticate(params[:password])
  end
end
