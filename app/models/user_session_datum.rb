class UserSessionDatum < ApplicationRecord
  belongs_to :user
  after_commit :check_for_duplicate_session, on: :create

  def check_for_duplicate_session
    data = UserSessionDatum.where(user_id: self.user_id).where.not(id: self.id)
    data.each do |session|
      session.destroy if(session.id != self.id)
    end
  end
end
