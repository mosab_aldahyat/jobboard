class Job < ApplicationRecord
  scope :active, -> { where(active: true) }
  scope :not_expired, -> { where("jobs.expire_date > :current_time", {current_time: Time.now}) }
  scope :search_by_title, -> (title) { where("jobs.title ILIKE '%#{title}%'") }
  scope :search_by_created_at, -> (created_at) { where(created_at: created_at) }

  has_many :job_applications

  validates :title, presence: true
  validates :description, presence: true
  validates :expire_date, presence: true
  validates :active, inclusion: { in: [true, false], message: "\"Status\" must either be true or false" }

  def destroy
    self.active = false
    self.save
  end

  def self.search params
    search_type = params[:search_type]
    query = params[:query]
    return nil unless search_type.present? && query.present?
    if search_type == "title"
      self.search_by_title(query).active
    else
      self.search_by_created_at(query).active
    end
  end
end
