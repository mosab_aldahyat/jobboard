require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module JobBoard
  class Application < Rails::Application
    config.exceptions_app = self.routes
    config.autoload_paths << Rails.root.join('lib')
    config.eager_load_paths << Rails.root.join('lib')
    config.paths['config/routes.rb'].concat Dir[Rails.root.join("config/routes/*.rb")]
    config.api_only = true
  end
end
