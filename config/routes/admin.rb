Rails.application.routes.draw do
  namespace :admins do
    post '/login' => 'sessions#create'
    post '/logout' => "sessions#destroy"
    resources :job_applications, only: [:index, :show]
    resources :jobs do
      collection do
        get :search
      end
    end
  end
end
