Rails.application.routes.draw do
  namespace :users do
    post '/login' => 'sessions#create'
    post '/logout' => "sessions#destroy"
    post '/signup' => 'users#create'
    resources :job_applications, only: [:index, :show, :create]
    resources :jobs, only: [:index, :show] do
      collection do
        get :search
      end
    end
  end
end
