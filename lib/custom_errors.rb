class CustomErrors
  def self.create_error(message)
    {error: {message: message}}
  end
end
