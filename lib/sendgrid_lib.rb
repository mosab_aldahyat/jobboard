require 'sendgrid-ruby'
include SendGrid
class SendgridLib
  FROM = 'support@mosab.com'
  NAME = "mosab"

  def self.send_email to_emails_list, substitutions_list, subject, text
    email_body = {
      "from": {
        "email": FROM,
        "name": NAME
      },
      "personalizations": map_lists(to_emails_list, substitutions_list),
      "subject": subject,
      "content": [
        {
          "type": "text/html",
          "value": text
        }
      ]
    }
    sg = SendGrid::API.new(api_key: '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')
    response = sg.client.mail._('send').post(request_body: email_body)
    puts "\n\n"
    puts response.status_code
    puts response.body
    puts response.headers
  end

  def self.map_lists to_emails_list, substitutions_list
    substitutions_list = [substitutions_list] if !substitutions_list.is_a? Array
    to_emails_list = [to_emails_list] if !to_emails_list.is_a? Array
     emails_hash = to_emails_list.map do |email|
       [{email: email}]
     end
     substitutions_list.zip(emails_hash).map do |sub_body, to_email|
      {
        "to": to_email,
        "substitutions": sub_body
      }
     end
  end
end
