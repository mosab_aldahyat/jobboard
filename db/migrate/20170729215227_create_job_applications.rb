class CreateJobApplications < ActiveRecord::Migration[5.0]
  def change
    create_table :job_applications do |t|
      t.belongs_to :user
      t.belongs_to :job
      t.string :status, default: "Not Seen"
      t.timestamps
    end
  end
end
