class AddExpireDateToJobs < ActiveRecord::Migration[5.0]
  def change
    add_column :jobs, :expire_date, :timestamp, null: false, default: Time.now, index: true
  end
end
