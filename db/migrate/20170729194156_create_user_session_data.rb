class CreateUserSessionData < ActiveRecord::Migration[5.0]
  def change
    create_table :user_session_data do |t|
      t.belongs_to :user
      t.timestamps
    end
  end
end
