class CreateAdminSessionData < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_session_data do |t|
      t.belongs_to :admin
      t.timestamps
    end
  end
end
